Astronaut Adventure: Side Scrolling 2-D Space Game

I. Description & Download
----------------------------
Astronaut-Adventure is a side-scrolling game developed in Python with the Pygame library. 
To run it, you must have both Python and Pygame downloaded, as well as all necessary game files in the same directory.


Python: https://www.python.org/downloads/
Pygame: http://www.pygame.org/download.shtml
		https://bitbucket.org/pygame/pygame/downloads/
		
Additionally, you can run the following arguments on the command line: 

		python get-pip.py
		python -m pip install wheel
		python -m pip install [pygame version]
		(for example, "python-m pip install pygame-1.9.2-cp27-cp27m-macosx_10_9_intel.whl")


II. Gameplay & Controls
----------------------------
The point of the game is to stay alive, while avoiding the falling meteors and collecting as many coins as possible.
The astronaut dies if the astronaut falls into a cliff or is hit 7 times.

Key          | Move
------------ | -------------
Left | Move left
Right | Move right
Up | Non-functional laser (for funsies)
Spacebar | Jump 





II. File List 
----------------------------
File       | Description 
------------ | -------------
game.py | Main game file 
gamebox.py | Library to help simplify Pygame interactions 		
README.md | This file 

Images (all currently hosted online, so original sources are not needed to run the game):

Image File       | Description 
------------ | -------------
astro3.png | Astronaut sprites
coin.png | Coin image
meteor.png | Meteor image
meteor2.png | Meteor image
sky.jpg | Sky image
spacefloor.png | Moon floor image
sprites.jpg | Astronaut sprites
sprites2.jpg | Astronaut sprites
start_screen.jpg | Game start screen
title.jpg | Game title screen


# astronaut-adventure-game
