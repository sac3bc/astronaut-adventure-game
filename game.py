#Current Version: Sesi Cadmus (sac3bc) 

import random
import pygame
import gamebox
#import pygame.mixer.music



screen = pygame.display.set_mode((800, 600)) 

#CLASSES

class Astronaut: 
    def __init__(self, sheet, rows, cols):
        self.rows = rows
        self.cols = cols
        self.sheet = gamebox.load_sprite_sheet("http://oi67.tinypic.com/23r6b9l.jpg", self.rows, self.cols) 
        self.frame = 0
        self.pace = 0
        self.obj = gamebox.from_image(60, 90, self.sheet[self.frame])
        self.laser = gamebox.from_color(self.obj.x + 30, self.obj.y + 2, "red", 100, 10 )


    def animate(self): 
        self.frame += 1
        self.pace += 1
        if self.frame == 10:
            self.frame = 0
        if self.pace % 4 == 0:
            self.obj.image = self.sheet[self.frame]



class LifeBar: 
    def __init__(self, max_lives):
        self.max_lives = max_lives
        self.life_amount = 100
        self.obj = gamebox.from_color(self.max_lives * 100 , 100, "red", self.life_amount, 20)



class Token: 

    def __init__(self, file):
        self.file = file 
        self.obj = gamebox.from_image(400,500, self.file)
        self.obj.scale_by(0.1)
        self.total = 0


class Astroid:

    def __init__(self, file):
        self.file = file 
        self.obj = gamebox.from_image(random.randint(0,800),100, self.file)
        self.obj.scale_by(.25)

    def astroid_move(self, yspeed):

        self.obj.yspeed = 10
        self.obj.y = self.obj.y + self.obj.yspeed


class Floor: 
     def __init__(self, x, y, file):
        self.x = x
        self.y = y
        self.file = file
        self.obj = gamebox.from_image(x, y, file) 
        self.size = 2000, 100


#FUNCTIONS

def meteor_hit(meteor_object, life_bar, character): 
    #global health 
    character.move(-50, -20)
    life_bar.life_amount -= 100/life_bar.max_lives
  
            
    meteor_object.x = random.randint(0,800)
    meteor_object.y = -100

#OBJECT INSTANTIATIONS

coin = Token("http://i65.tinypic.com/qoapmc.png")
meteor3 = Astroid("http://i63.tinypic.com/2agvxi9.png")
meteor4 = Astroid("http://i63.tinypic.com/2agvxi9.png")
health_bar = LifeBar(7)
player1 = Astronaut("http://oi67.tinypic.com/23r6b9l.jpg", 1, 10)
ground = Floor(-100, 600, "http://oi65.tinypic.com/2eyzy8h.jpg") 
ground2 = Floor(1800, 600, "http://oi65.tinypic.com/2eyzy8h.jpg")

'''
VARIABLE DECLARATIONS

camera: game window
platforms: list containing moving platforms
yplaform: 
night: background image
intro: boolean to indicate we are in the start screen / instructions phase
timer: used to allocate time for startscreen and instructions
start, rest: booleans used to assist in alternating the two ground platforms
ground1_turn, ground_2 turn: boolean to signify which platform is currently in view
start1screen: the first game screen with the image from gameplay
start2screen: screen containing instructions
background_music: play techno during the start of the game 
laser_music: the sound when we fire the laser 

'''

camera = gamebox.Camera(800,600)

platforms = [gamebox.from_color(200,random.randint(300, 500), "green", 100, 5),
    gamebox.from_color(400, random.randint(300, 500), "green", 100, 5),
    gamebox.from_color(600, random.randint(300, 500), "green", 100, 5),
    gamebox.from_color(800, random.randint(300, 500), "green", 100, 5)]

night = gamebox.from_image(0,0, "http://oi66.tinypic.com/2itm0yc.jpg")
night.center = (400, 300)
start = True
intro = False
timer = 0
rest = False
move = True
ground2_turn = False
ground1_turn = False

start1screen = gamebox.from_image(0, 0, "http://oi63.tinypic.com/10wqssl.jpg")
start2screen = gamebox.from_image(0, 0, "http://oi65.tinypic.com/2w5kt4w.jpg")
start1screen.center = (400, 300)
start2screen.center = (400, 300)

pygame.mixer.music.load("acid_techno.ogg")
#import pdb; pdb.set_trace()
laser_music = gamebox.load_sound("laser_sound.wav")

'''

Tick is a function that contains the event loop for each frame. 
Requires the paramter "keys"
All game game behavior wil be defined in this function 

'''


def tick(keys):

    global rest
    global ground2_turn
    global ground1_turn
    global start
    global intro
    global timer
    global meteor3
    global meteor4

    
    
    '''
    INTRO

    During this portion, the start screen and instructions are played
    Timer is updated 30 times per second (30 ticks per second)
    Intro should show for the first 2 seconds
    Instructions should appear for the next 10 

    '''



    if intro:
        if 60 - (timer // 30) > 58:
            camera.draw(start1screen)
        if 60 - (timer // 30) < 58 and 60 - (timer // 30) > 48:
            camera.draw(start2screen)
        if 60 - (timer // 30) < 48:
            start = True
            intro = False


    '''
    START
    
    '''

    if start:

        pygame.mixer.music.play(-1)

        #draw the background 
        
        camera.draw(night)

        


        ground.obj.x -= 3
     

        '''
        CHARACTER BEHAVIOR

        Controls (left, right, jump, laser)
        Stops character from falling through ground 
        Animate character

        '''

        if pygame.K_RIGHT in keys:
            player1.obj.x += 10
        if pygame.K_LEFT in keys:
            player1.obj.x -= 10
        if pygame.K_UP in keys:
                player1.laser.x = player1.obj.x + 30
                player1.laser.y = player1.obj.y - 20
                camera.draw(player1.laser)
                laser_music.play(1)


        #prevents character from falling through ground

        if player1.obj.bottom_touches(ground.obj):
            player1.obj.yspeed = 0
            if pygame.K_SPACE in keys:
                        player1.obj.yspeed = -20


        if player1.obj.bottom_touches(ground2.obj):
            player1.obj.yspeed = 0
            if pygame.K_SPACE in keys:
                        player1.obj.yspeed = -20

        if player1.obj.touches(ground.obj):
            player1.obj.move_to_stop_overlapping(ground.obj)
        if player1.obj.touches(ground2.obj):
            player1.obj.move_to_stop_overlapping(ground2.obj)

        #control gravity, speed vertically    

        player1.obj.yspeed += 1
        player1.obj.y = player1.obj.y + player1.obj.yspeed

        #animation

        player1.animate()
        

        '''

        PLATFORM MOVEMENT

        We have ground and ground2 which alternate on the screen 
        When ground reaches a certain x value on the screen 


        '''

        
        ground2.obj.x -=3

        #if ground.obj.x < -450:
        # ground2.obj.x -=3
        if ground.obj.x < -2700:
            ground.obj.x = 1800
            ground.obj.x -=3
            move = False #start
            rest = True
            ground2_turn = True

        if rest:

            if ground2_turn:
                if ground2.obj.x < -450:
                    ground2.obj.x -=3
                if ground2.x < -2700:
                    ground2.obj.x = 1800
                    ground2.obj.x -=3
                    ground.obj.x -=3
                    ground2_turn = False
                    ground1_turn = True
            if ground1_turn:
                if ground.obj.x < -450:
                    ground2.obj.x -=3
                if ground.x < -2700:
                    ground.obj.x = 1800
                    ground.obj.x -=3
                    ground1_turn = False
                    ground2_turn = True




        if player1.obj.touches(coin.obj):
            coin.total += 1
            coin.obj.x = random.randint(0,800)
            coin.obj.y = random.randint(350,550)


        if player1.obj.y > 900:
            health_bar.life_amount -= 50
            health_bar.obj = gamebox.from_color(700,120,"red",health_bar.life_amount,20)
            player1.obj.x = 400
            player1.obj.y = 250
            player1.obj.yspeed = -5


        '''
        METEOR BEHAVIOR 

        Randomly regenerates meteor once it falls too low
        Diminishes players health bar if comes into contact with player


        '''


        if meteor3.obj.y == 700:
            meteor3.obj.y = -100
            meteor3.obj.x = random.randint(0, 800)
        if meteor4.obj.y == 700:
            meteor4.obj.y = -100
            meteor4.obj.x = random.randint(0, 800)


        coin.obj.x -= 3 #needs to be still with screen 
        if coin.obj.x < 0 or coin.obj.x > 800:
            coin.obj.x = random.randint(0,800)


        if player1.obj.touches(meteor3.obj):
            meteor_hit(meteor3.obj, health_bar, player1.obj)
        if player1.obj.touches(meteor4.obj):
            meteor_hit(meteor4.obj, health_bar, player1.obj)



        meteor3.astroid_move(10)
        meteor4.astroid_move(10)


        '''
        PLATFORM BEHAVIOR

        Ensures that character does not fall through 
        Moves platform across scrolling screen 
        Regenerates them once they hit a certain x value 

        '''

        for platform in platforms:
          
            if player1.obj.bottom_touches(platform):
                player1.obj.yspeed = 0
                if pygame.K_SPACE in keys:
                    player1.obj.yspeed = -20

            if player1.obj.touches(platform):
                player1.obj.move_to_stop_overlapping(platform)

            #move at rate of other objects

            platform.x -=3

            #make more platforms once it reaches a certain place onscreen
            if platform.x < -50:
                platform.x = 850
                platform.y = random.randint(350, 500)




        #ON SCREEN DISPLAYS 

        coin_count = gamebox.from_text(50,100,str(coin.total),"Times New Roman",30,"green")
        health_text = gamebox.from_text(700,80,"Health:","Times New Roman",20,"red")
        coin_text = gamebox.from_text(50,80,"Coins:","Times New Roman",20,"green")
        health_bar.obj = gamebox.from_color(700,120,"red",health_bar.life_amount,20)

        

        

  



        
        #DRAW ALL OBJECTS


        camera.draw(player1.obj)
        camera.draw(coin.obj)
        camera.draw(health_bar.obj)
        camera.draw(health_text)
        camera.draw(coin_text)
        camera.draw(coin_count)
        camera.draw(meteor3.obj)
        camera.draw(meteor4.obj)
        camera.draw(ground.obj)
        camera.draw(ground2.obj)
        for platform in platforms:
            camera.draw(platform)



        #GAME OVER

        if health_bar.life_amount <= 0:
            camera.clear("black")
            you_win = gamebox.from_text(400,300,"Game Over! You got " + str(coin.total) + " coins!","Times New Roman",40,"green")
            camera.draw(you_win)



    #UPDATE TIMER, SHOW DISPLAY 

    timer +=1
    camera.display()

TICKERS_PER_SECOND = 30

# keep this line the last one in your program
gamebox.timer_loop(TICKERS_PER_SECOND, tick)